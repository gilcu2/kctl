#!/usr/bin/env python

import os
import yaml
import sys
import argparse
from typing import *

YAML = Union[list, Iterator, dict, str]


def parse_arguments(argv: list[str]):
    parser = argparse.ArgumentParser()
    parser.add_argument('yaml', help='YAML file')
    parser.add_argument('-e', action='append', nargs=1, dest='vars', help='Environment var')

    return parser.parse_args(argv)


def get_vars(parsed_vars: list[list[str]]) -> dict:
    def process_var(s: str) -> tuple[str, str]:
        parts = s.split('=')
        name = parts[0]
        value = os.environ[name] if len(parts) == 1 else parts[1]
        return name, value

    return dict(
        map(lambda x: process_var(x[0]), parsed_vars)
    )


def read_yaml(path: str) -> Iterator:
    with open(path) as file:
        return list(yaml.load_all(file, Loader=yaml.CLoader))


def evaluate(data: YAML, vars: dict) -> YAML:
    if type(data) is str:
        data = data.format_map(vars)
    elif type(data) is list:
        data = [evaluate(e, vars) for e in data]
    elif type(data) is dict:
        for key in data.keys():
            data[key] = evaluate(data[key], vars)

    return data


def main(argv: list[str]):
    parsed_args = parse_arguments(argv[1:])
    vars = get_vars(parsed_args.vars)
    yaml_data = read_yaml(parsed_args.yaml)
    evaluated_yaml = evaluate(yaml_data, vars)
    print(yaml.dump_all(evaluated_yaml, Dumper=yaml.CDumper))


if __name__ == '__main__':
    main(sys.argv)
