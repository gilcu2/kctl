from evalyaml import *
import os
import yaml


def test_parse_args():
    # Given arguments
    argv = ['-e', 'VAR1', '-e', 'VAR2=TEST', 'my_yaml.yaml']

    # When parsed
    parsed_args = parse_arguments(argv)

    # Then the results are the expected
    assert parsed_args.vars == [['VAR1'], ['VAR2=TEST']]
    assert parsed_args.yaml == 'my_yaml.yaml'


def test_get_vars():
    # Given the parsed arguments
    parsed_args = [['VAR1'], ['VAR2=TEST']]

    # And setting the correspondeng env var
    os.environ['VAR1'] = 'TEST1'

    # When get the vars
    vars = get_vars(parsed_args)

    # Then the values are the expected
    assert vars['VAR1'] == 'TEST1'
    assert vars['VAR2'] == 'TEST'


def test_evaluate():
    # Given a yaml
    yaml_data = list(yaml.load_all('''
apiVersion: v1
kind: Pod
metadata:
  name: envar-demo
  labels:
    purpose: demonstrate-envars
spec:
  containers:
  - name: envar-demo-container
    image: "{REPO}/node-hello:1.0"
    env:
    - name: DEMO_GREETING
      value: "Hello from the environment {ENV}"
    - name: DEMO_FAREWELL
      value: "Such a sweet sorrow {DEST}"
      
---
  
ingests:
  - timestamp: "{YEAR}-01-01T00:00:00.000Z"
    id: OverheadDistributionLineSegment_31168454  
    '''))

    # And vars
    vars = {
        'REPO': 'localhost:401',
        'ENV': 'local',
        'DEST': 'tester',
        'YEAR': '1970'
    }

    # When evaluate the vars
    r = evaluate(yaml_data, vars)

    # Then the results have the vars substituted
    assert r == list(yaml.load_all('''
apiVersion: v1
kind: Pod
metadata:
  name: envar-demo
  labels:
    purpose: demonstrate-envars
spec:
  containers:
  - name: envar-demo-container
    image: localhost:401/node-hello:1.0
    env:
    - name: DEMO_GREETING
      value: "Hello from the environment local"
    - name: DEMO_FAREWELL
      value: "Such a sweet sorrow tester"
---
  
ingests:
  - timestamp: '1970-01-01T00:00:00.000Z'
    id: OverheadDistributionLineSegment_31168454  
      
    '''))
